# Extension [Quarto](https://quarto.org/) for MIAME pole

This repository was initiated with:

``` shell
quarto create extension format:pdf
```

## Installing

### For a new project

```bash
quarto use template https://forgemia.inra.fr/pole-migrateurs/modeles/quarto-extension/-/archive/0.1.1/quarto-extension-0.1.1.zip
```

This will install the extension and create an example qmd file that you can use as a starting place for your article.

### For an existing project or to update the extension

```bash
quarto add https://forgemia.inra.fr/pole-migrateurs/modeles/quarto-extension/-/archive/0.1.1/quarto-extension-0.1.1.zip
```

## Using

This document would be rendered as:
![example](template.png)

If you do not need the partner logo just delete the following:
~~~yml
logoPartenaire: mon_partenaire.jpg
logoPartenaireTaille: 1.50cm
urlPartenaire: www.partenaire.fr
~~~

## Example

Here is the source code for a minimal sample document: [template.qmd](template.qmd).
It will be used when you use the ```quarto use template``` command.
